from enum import Enum


class ExecutionState(Enum):
    NOT_INITIATED = "not initiated"
    PRECONDITIONS = "preconditions"
    ROUTING = "routing"
    EXECUTING = "executing"
    CONFIRMATION = "confirmation"
    COMPLETED = "completed"


class ConfirmationStatus(Enum):
    NOT_AVAILABLE = "not available"
    CONFIRMED = "confirmed"
    NOT_CONFIRMED = "not confirmed"
    ABORTED = "aborted"


class ContinuationAction(Enum):
    RESUME = "resume"
    ABORT = "abort"
    RESTART = "restart"
    ASK_USER = "ask user"
    RAISE_EVENT = "raise event"
    CONTINUE = "continue"
    TERMINATE = "terminate"
