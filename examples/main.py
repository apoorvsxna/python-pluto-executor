import time
import threading

#import pluto_parser


class PlutoEngine:

    def __init__(self):
        self._loaded_procedures = {}

    def load(self, filename):
        filename += ".py"
        with open(filename, 'r') as f:
            script = f.read()

        procedure_name = filename.split(".")[0]
        self._loaded_procedures[procedure_name] = {
            "script": script,
            "thread": None,
            "procedure": None,
        }
        return procedure_name

    def list_loaded_procedures(self):
        pass

    def run(self, procedure_name):
        script = self._loaded_procedures[procedure_name]["script"]
        exec(script, locals())
        procedure = locals()["Procedure_"]()
        t = threading.Thread(target=procedure.run)
        self._loaded_procedures[procedure_name]["thread"] = t
        self._loaded_procedures[procedure_name]["procedure"] = procedure
        t.start()

    def suspend(self, procedure_name):
        pass

    def resume(self, procedure_name):
        pass

    def has_completed(self, procedure_name):
        t = self._loaded_procedures[procedure_name]["thread"]
        p = self._loaded_procedures[procedure_name]["procedure"]
        print(p.confirmation_status)
        return not t.is_alive()


###############################################################

import logging
logging.basicConfig(level=logging.DEBUG)


pluto_engine = PlutoEngine()
print("Running: ", pluto_engine.load("dummy"))
#pluto_engine.list_loaded_procedures()
pluto_engine.run("dummy")
print(".")
#pluto_engine.suspend("dummy.py")
#pluto_engine.resume("dummy.py")
time.sleep(1)

while not pluto_engine.has_completed("dummy"):
    print(".")

    time.sleep(1)

print("completed")
