# Python Pluto Engine

A Python module for executing and scheduling PLUTO scripts.

## Getting Started

Install the module:

```bash
$ git clone https://gitlab.com/librecube/prototypes/python-pluto-engine
$ cd python-pluto-engine
$ python -m venv venv
$ source venv/bin/activate
(venv) $ pip install -e .
```

Go to the examples folder and run `main.py`.

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-pluto-engine/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-pluto-engine

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
